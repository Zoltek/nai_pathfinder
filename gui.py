import tkinter
from tkinter import *
from astar import *

SIZE = 10


class Maze(object):

    def __init__(self, grid_size):

        self.x = grid_size
        self.y = grid_size
        self.buttons = [[object] * self.x for i in range(self.y)]
        self.walls = []
        self.s = 0
        self.e = 0
        self.expensive_diagonal = False
        self.root = tkinter.Tk()
        self.frame = tkinter.Frame(self.root)
        self.init_maze()

    def init_maze(self):

        tkinter.Grid.rowconfigure(self.root, 0, weight=1)
        tkinter.Grid.columnconfigure(self.root, 0, weight=1)

        self.frame.grid(row=0, column=0, sticky=N + S + E + W)
        self.root.title("A*")

        for x in range(self.x):
            for y in range(self.y):
                Grid.columnconfigure(self.frame, y, weight=0)
                self.buttons[x][y] = tkinter.Button(self.frame, text=" ", borderwidth=1,
                                                    command=lambda
                                                        x=x, y=y: self.switch_button(x, y), height=3, width=6)
                self.buttons[x][y].grid(row=x, column=y, sticky=N + S + E + W)
        self.find_button = Button(self.frame, text="FIND", borderwidth=3, command=lambda: self.find())\
            .grid(row=x+1,
                  column=0,
                  columnspan=2,
                  sticky=N + S + E + W)

        self.reset_button = Button(self.frame, text="RESET", borderwidth=3, command=lambda: self.reset()).\
            grid(row=x+1,
                 column=y-1,
                 columnspan=2,
                 sticky=N + S + E + W)
        self.chose_diagonal_cost = Checkbutton(self.frame, text='expensive diagonal: ',
                                               command=lambda: self.change_cost()).grid(row=x+1,
                                                                                        column=y-5,
                                                                                        columnspan=3,
                                                                                        sticky=N + S + E + W)

        self.default_color = self.buttons[0][0].cget("background")

        self.root.mainloop()

    def change_cost(self):
        if self.expensive_diagonal:
            self.expensive_diagonal = False
        else:
            self.expensive_diagonal = True
    def switch_button(self, x, y):

        if self.buttons[x][y]["text"] == " " and self.s == 0:
            self.buttons[x][y]["text"] = "START"
            self.buttons[x][y]["bg"] = "red"
            self.s = 1
            self.start = (x, y)
        elif self.buttons[x][y]["text"] == "START" and self.s == 1 and self.e == 0:
            self.buttons[x][y]["text"] = "END"
            self.buttons[x][y]["bg"] = "red"
            self.s = 0
            self.e = 1
            self.end = (x, y)
        elif self.buttons[x][y]["text"] == "END" and self.e == 1 and self.s == 0:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.e = 0
        elif self.buttons[x][y]["text"] == " " and self.s == 1 and self.e ==0:
            self.buttons[x][y]["text"] = "END"
            self.buttons[x][y]["bg"] = "red"
            self.s = 1
            self.e = 1
            self.end = (x, y)
        elif self.buttons[x][y]["text"] == "END" and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.s = 1
            self.e = 0
        elif self.buttons[x][y]["text"] == "START" and self.s == 1 and self.e == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.s = 0
            self.e = 1
        elif self.buttons[x][y]["text"] == " " and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = "W"
            self.buttons[x][y]["bg"] = "black"
            self.walls.append((x, y))

        elif self.buttons[x][y]["text"] == "W" and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.walls.remove((x, y))

    def find(self):

        if self.s == 1 and self.e == 1:
            astar = Astar()
            path = astar.resolve(self.x, self.walls, self.start, self.end, self.expensive_diagonal)
            if path == None:
                Label(self.frame, text="Endpoint is unreachable!", borderwidth=0, fg="red")\
                    .grid(row=self.x, column=self.y - 8, columnspan=6, sticky=N + S + E + W)
                return
            for x in range(self.x):
                for y in range(self.y):
                    if (x, y) in path and (x,y) != self.start and (x, y) != self.end:
                        self.buttons[x][y] = tkinter.Button(
                            self.frame, bg="green", borderwidth=1, command=lambda x=x, y=y: self.switch_button(x, y),
                            height=3, width=6)
                        self.buttons[x][y].grid(row=x, column=y, sticky=N + S + E + W)
                        Label(self.frame, text="", borderwidth=0).grid(row=self.x,
                                                                       column=self.y if self.y > 2 else 2,
                                                                       columnspan=6,
                                                                       sticky=N + S + E + W)

        else:
            Label(self.frame, text="Set start and end point!", borderwidth=0, fg="red")\
                .grid(row=self.x,
                      column=self.y if self.y > 2 else 2,
                      columnspan=6,
                      sticky=N + S + E + W)

    def reset(self):

        for x in range(self.x):
            for y in range(self.y):
                if self.buttons[x][y]["bg"] == "green":
                    self.buttons[x][y]["text"] = " "
                    self.buttons[x][y]["bg"] = self.default_color


if __name__ == '__main__':
    Maze(SIZE)
