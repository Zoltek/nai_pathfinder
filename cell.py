class Cell(object):
    def __init__(self, x, y, wall):
        self.wall = wall
        self.x = x
        self.y = y
        self.parent = None
        self.g = 0              # koszt ruchu od komorki poczatkowej
        self.h = 0              # heurystyka
        self.f = 0              # f = g + h

    def __lt__(self, other):
        return self.f < other.f
